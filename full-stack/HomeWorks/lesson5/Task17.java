package HomeWorks.lesson5;

import java.util.*;

class Task17
{


    public static void main(String[] args)
    {
        Integer arr[] = {1,3,5,7,9};
        System.out.println("Original Array:" + Arrays.asList(arr));


        // Collections.reverse(Arrays.asList(myArray));
        // System.out.println("Reversed Array:" + Arrays.asList(myArray));

        for(int i = 0;i<arr.length/2;i++){
            int t = arr[i];
            arr[i] = arr[arr.length-1-i];
            arr[arr.length-1-i] = t;
        }

        for(int i = 0;i<arr.length;i++){
            System.out.print(arr[i] + " ");
        }
    }
}

