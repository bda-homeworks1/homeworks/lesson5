package HomeWorks.lesson5;

import java.util.Scanner;

public class Task1
{
    public static void main(String[] args) {
        System.out.println("5 reqem daxil edin");
        int[] arr = new int[5];
        Scanner sa = new Scanner(System.in);

        for(int i=0;i<5;i++){
            arr[i] = sa.nextInt();
        }

        for(int i = 0; i<5;i++){
            for(int j = i+1; j<5;j++){
                if(arr[j]<arr[i]){
                    int t = arr[i];
                    arr[i] = arr[j];
                    arr[j] = t;
                }
            }
        }

        System.out.println("arrayin sort olunmus hali:");
        System.out.print("[");
        for(int i = 0; i<arr.length;i++){
            System.out.print(arr[i]);
            if(i!=arr.length-1){
                System.out.print(", ");
            }
        }
        System.out.print("]");
    }
}